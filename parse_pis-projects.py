import argparse
from itertools import chain
from pathlib import Path
import re

def _parse_cli():
    args = argparse.ArgumentParser()
    args.add_argument("--root_directory", default="")
    return args.parse_args()

args = _parse_cli()

source_file = Path(args.root_directory) / "metadata/pis-projects.tex"
output_file = Path(args.root_directory) / "data/crcResearchAreas.csv"

# read in the file
with open(source_file, 'r', encoding="utf-8") as f:
    my_data = f.read()

# locate all data that appears in the "area" tags
all_areas = [x.split(", ") for x in re.findall(r"area={([0-9-, ]+)", my_data)]

# we get back a list of lists, flatten it and create a set to both remove
# duplicates and sort them alphabetically
areas = set(chain.from_iterable(all_areas))

# write output as a csv. There's no column header or other data, so
# writelines is sufficient
with open(output_file, 'w') as f:
    f.writelines([area + '\n' for area in sorted(areas)])
