#author: Lukas C. Bossert
#mail: bossert@itc.rwth-aachen.de
PROJECT=crc
PROPOSAL=$(PROJECT)-proposal
CV=$(PROJECT)-cv
BIBFILE ?="bib/crc.bib"
#export TEXINPUTS:=.//:

FILE?=
MEDIA=img/$(FILE)

SHELL = bash
MAKE  = make
LATEX  =lualatex
COMPILER_INFO=$(shell $(LATEX)  -v | head -n1 | cut -d ' ' -f3-) [$(VERS)]
LATEX_OPTS   =\newcommand*\InfoTeX{$(COMPILER_INFO)}
LATEXMK = latexmk -$(LATEX) -g -usepretex="$(LATEX_OPTS)"

# zip
PWD   = $(shell pwd)
TEMP := $(shell mktemp -d -t tmp.XXXXXXXXXX)
TDIR  = $(TEMP)/$(PROJECT)
VERS  = $(shell TZ=Europe/Berlin /bin/date "+%Y-%m-%d@%H-%M-%S")
DATE  = $(shell TZ=Europe/Berlin /bin/date "+%Y-%m-%d")
# Colors
RED   = \033[0;31m
CYAN  = \033[0;36m
NC    = \033[0m
echoPROJECT = @echo -e "$(CYAN) <$(PROJECT)>$(RED)"

.PHONY:  all  crc-proposal cv



all:
	$(echoPROJECT) "* compiling proposal * $(NC)"
	time $(LATEXMK) $(PROPOSAL).tex
	open $(PROPOSAL).pdf
	$(echoPROJECT) "* proposal compiled * $(NC)"

cv:
	$(echoPROJECT) "* compiling cv * $(NC)"
	time $(LATEXMK) $(CV).tex
	open $(CV).pdf
	$(echoPROJECT) "* cv compiled * $(NC)"


$(PROPOSAL).pdf:
	$(MAKE) all

$(CV).pdf:
	$(MAKE) cv

prepare: research-areas networks

# going through all the
research-areas:
	$(echoPROJECT) "* start getting research areas * $(NC)"
	perl -pe 's/},/\n/gm' "metadata/""crc-metadata-proposal.tex" \
| perl -pe 's/ //gm' \
| grep -h '^area' \
| perl -pe 's/area={//gm' \
| perl -pe 's/,/\n/gm'  \
| sort -u \
| grep '\d' | tee "data/""crcResearchAreas.csv"
	cat "data/""crcResearchAreas.csv"
	$(echoPROJECT) "* getting research areas done * $(NC)"



CONTENTFOLDER="content/"
PLOTFOLDER="plots/"
PROJECTSNETWORK="crc-projects-network"
COLLABORATIONFILE=$(PLOTFOLDER)$(PROJECTSNETWORK).csv
PROJECTSFILE=$(PLOTFOLDER)$(PROJECTSNETWORK).r
# creating the collaboration network plots for the chapter
# "Role within the CRC"
networks: projects-network publications-network

projects-network:
# first get all the data from the files.
# Going through all files from the =content= folder and
# collect the information.
# Intended collaborations within the CRC must be
# displayed as following: =%>>OWN-PROJECT-ID,OTHER-PROJECT-ID=,
#
# e.g. =%>>B06,A01=.
	$(echoPROJECT) "* start getting project edges * $(NC)"
	echo "from,to" > $(COLLABORATIONFILE)
	grep -h "%>>" $(CONTENTFOLDER)03-{A,B,Q}*.tex \
	| perl -pe 's/%>>//gm' >> $(COLLABORATIONFILE)
	Rscript $(PROJECTSFILE)
	$(echoPROJECT) "* getting project edges done * $(NC)"


PUBLICATIONSFOLDER="publications/"
PUBLICATIONSFILE=$(PLOTFOLDER)"crc-publications-network.r"
publications-network:
	$(echoPROJECT) "* start getting publication edges/nodes * $(NC)"
	echo "Run script locally"
	Rscript $(PUBLICATIONSFILE)
	$(echoPROJECT) "* getting publication edges/nodes done * $(NC)"

count.bib:
	bibtool \
	-r biblatex \
	--count.all=on \
	$(BIBFILE)

#http://www.gerd-neugebauer.de/software/TeX/BibTool/
#
clean.bib: clean.bib.crc
	bibtool -r biblatex -o  $(FILE)

clean.bib.temp:
		betterbib update -iu bib/temp.bib
		$(MAKE) bibtool FILE=bib/temp.bib

clean.bib.crc:
	$(MAKE) bibtool FILE=$(BIBFILE)


bibtool:
	$(echoPROJECT) "* start cleaning bib-file  $(FILE) * $(NC)"
	bibtool \
  --print.align=20 \
  --print.align.key=0 \
  --print.align.string=20 \
  --print.equal.right=on \
  --print.wide.equal=on \
  --print.deleted.entries=off \
  --print.use.tab=off \
  --symbol.type=lower \
  -s \
	-sort.format="%s(doi)" \
  --check.double=on \
  --'rename.field {journal=journaltitle}' \
  --'delete.field{abstract}' \
  --'delete.field{journalsubtitle}' \
  --'delete.field{month}' \
  --'delete.field{publisher}' \
  --'delete.field{ISSN}' \
  --'delete.field{source}' \
  --'delete.field{language}' \
  --'delete.field{url}' \
  --'delete.field{copyright}' \
  --'delete.field{location}' \
  --preserve.key.case=on \
  $(FILE) \
  -r biblatex \
  -o  $(FILE)
	$(echoPROJECT) "* bib-file $(FILE) cleaned * $(NC)"


betterbib:
	betterbib update -iu $(FILE)

update.bib:
	$(MAKE) betterbib FILE=$(BIBFILE)
	$(MAKE) bibtool FILE=$(BIBFILE)


clean.all:
	$(echoPROJECT) "* start cleaning * $(NC)"
	rm $(PROJECT)*.{blg,run.xml,toc,dep,fls,log,oai,bcf,bbl,aux,fdb_latexmk}
	rm content/*.aux
	$(echoPROJECT) "* all cleaned * $(NC)"

compress:
	gs \
	-sDEVICE=pdfwrite \
	-dCompatibilityLevel=1.5 \
	-dPDFSETTINGS=/printer \
	-dNOPAUSE \
	-dQUIET \
	-dBATCH \
	-sOutputFile=$(PROPOSAL)-compressed.pdf \
	$(PROPOSAL).pdf
