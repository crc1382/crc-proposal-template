import argparse
import datetime
import logging
from pathlib import Path
import shutil

from jinja2 import Environment, FileSystemLoader

logger = logging.getLogger(__name__)

ENVIRONMENT = Environment(loader=FileSystemLoader("templates/"))
MAIN_TEMPLATE = ENVIRONMENT.get_template("main.jinja2_template.html")
PLOTS_TEMPLATE = ENVIRONMENT.get_template("images.jinja2_template.html")
CHAPTER_TEMPLATE = ENVIRONMENT.get_template("chapters.jinja2_template.html")


def _parse_cli():
    args = argparse.ArgumentParser()
    args.add_argument("--root_dir", default="")
    args.add_argument(
        "--repo_address",
        default=("https://git.rwth-aachen.de/crc1382/crc-proposal-template/"),
    )
    return args.parse_args()


def get_current_time(server_offset=datetime.timedelta(hours=2)):
    return datetime.datetime.now() + server_offset


def build_plot_page(root_dir, directory):
    logger.info("build page for %s...", directory)
    logger.debug("searching for images in %s...", f"plots/{directory}/*.png")
    images = [f for f in Path(root_dir).glob(f"plots/{directory}/*.png")]

    for img_path in images:
        logger.debug(img_path)

    image_page = PLOTS_TEMPLATE.render(
        directory=directory,
        time=get_current_time().strftime("%d.%m.%Y %H:%M:%S"),
        images=[f"{directory}/{image.name}" for image in images],
    )

    output_file = Path(f"out/plots/{directory}.html")
    output_file.parent.mkdir(exist_ok=True, parents=True)
    with open(output_file, "w", encoding="utf-8") as f:
        f.write(image_page)


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    args = _parse_cli()
    logger.info(args)

    output_directory = Path("public")
    output_directory.mkdir(exist_ok=True)

    # Write the Landing Page =================================================
    logger.info("building main page...")

    all_jobs = [
        ("container", "container"),
        ("pull", "pull"),
        ("plots", "rplots"),
        ("prepare", "prepare"),
        ("prepare", "prepare_parse_tex"),
        ("latexmk", "cv"),
        ("latexmk", "proposal"),
    ]

    stage_status = {k: "failure-red" for k in all_jobs}
    print(stage_status)

    with open("stage_results", "r") as f:
        for line in f.readlines():
            print(line)
            stage, job, job_status = line.strip().split(",")
            job = job.replace("-", "_")
            if (stage, job) in stage_status and job_status == "success":
                stage_status[(stage, job)] = "success-green"
                print(f"found {(stage, job)} - setting to success")
            else:
                print(f"{(stage, job)} not found or not success")

    main_page = MAIN_TEMPLATE.render(
        time=get_current_time().strftime("%d.%m.%Y %H:%M:%S"),
        root_dir=args.root_dir,
        stage_status=stage_status,
    )
    logger.info("writing index.html...")
    with open("public/index.html", "w", encoding="utf-8") as f:
        f.write(main_page)

    # chapters_page = CHAPTER_TEMPLATE.render(
    #     time=get_current_time().strftime("%d.%m.%Y %H:%M:%S"),
    #     chapter_files=[f for f in Path("chapter").glob("*.pdf")],
    # )
    # logger.info("writing chapters.html")
    # with open("public/chapters.html", "w", encoding="utf-8") as f:
    #     f.write(chapters_page)

    # Write the two Plot Pages ===============================================
    build_plot_page(args.root_dir, "projects")
    build_plot_page(args.root_dir, "publications")

    logger.info("DONE!")
