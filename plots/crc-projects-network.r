setwd("plots/")
# Load the edges and nodes table files
crcBaseFile <- "crc-projects-network.csv"
crc_edges <- read.csv(crcBaseFile)

crc_nodes <- read.table(header=TRUE,
                        sep=",",
                        text="
name,group
A01,A
A02,A
A03,A
A04,A
B01,B
B02,B
B03,B
B04,B
Q01,Q
Q02,Q
Q03,Q
")


if (!require("igraph")) install.packages("igraph")
if (!require("ggraph")) install.packages("ggraph")

# Construct the network object and remove multiple edges with simplify
#net_crc <- simplify(graph_from_data_frame(
net_crc <- graph_from_data_frame(
  d = crc_edges,        # d = data frame =~ edges
  vertices = crc_nodes, # nodes
  directed = FALSE)

# Set up the color palette for the CRC groups
crc_color_code <- c("A" = "#00549f", # blau/blue
                    "B" = "#57AB27", # grün/green
                    "Q"= "#9C9E9F" # grau/grey
)


# Define a function for plotting a circular collaboration plot
get_collab_plot <- function(
                            network,
                            crc_name,
                            crc_palette,
                            print_all_edges = TRUE,
                            xy_lim= 1.1
                            ){
  # network: an igraph object with the collaborations
  # crc_name: a character indicating which nodes should be the focus (should belong to V(network)$name)
  # crc_palette: a (named list) of CRC groups with their associated colors
  # print_all_edges: a boolean flag indicating whether the edges not linked to the focuses node should be displayed
  # xy_lim: a numerical indicating the xy limits of the plot for aesthetics
  
  # Identify the edges connected to the focus node
  edges_group <- incident(network, crc_name)
  # Label the corresponding edges in the igraph object
  E(network)$focus <- E(network) %in% edges_group
  
  # Create a circular layout and sort by name
  l <- create_layout(
    network,
    layout = "linear",
    circular = TRUE,
    sort.by=name
  )
  
  # Change the position of the label according to its position on the circle
  # based on https://gist.github.com/ajhmohr/5337a5c99b504e4a243fad96203fa74f
  l$angle <-with(l,ifelse(atan(-(x/y))*(180/pi) < 0,
                          90 + atan(-(x/y))*(180/pi),
                          270 + atan(-x/y)*(180/pi)))
  l$hjust<-with(l,ifelse(x < 0,1.15,-0.15))
  
  # Set the base plot
  p <- ggraph(l)
  
  # When printing all edges is asked, do it
  if(print_all_edges) {
    p <- p + geom_edge_arc(aes(filter = (!focus)), width= .5, alpha=0.3, color="gray70", lineend = "round") 
  }
  
  p <- p +
    # Add the edges connected to the focus node in red (complementary to the selected blue)
    # alpha = density of edge: 1>n>0;
    geom_edge_arc(aes(filter = (focus)), width=1, alpha=1, color="#9E1500", lineend = "round") +
    # Print all other nodes
    geom_node_text(aes(filter = (name!=crc_name),
                       label=name, angle=angle, hjust=hjust, color=group), fontface="plain", size= 4)+
    # Print the focus node in bold and red
    geom_node_text(aes(filter = (name==crc_name),
                       label=name, angle=angle, hjust=hjust), color="#9E1500", fontface="bold", size= 4)+
    # print the nodes according to the color palette
    scale_color_manual(values = crc_palette)+
    coord_fixed(xlim = c(-xy_lim,xy_lim),ylim = c(-xy_lim,xy_lim))+theme_void()+
    theme(legend.position = "none")
  return(p)
}

# Example code for one group
#plot_exA <- get_collab_plot(network = net_crc, crc_name = "A05", crc_palette = crc_color_code)
#plot_exB <- get_collab_plot(network = net_crc, crc_name = "A05", crc_palette = crc_color_code, print_all_edges = F)

# ggsave("crc-CIRCLE-plot.png",
#        plot_exA, # plot_exB
#        width = 10, height =10,
#        units = "cm")

# for all the nodes
for (focus in V(net_crc)$name) {
  # First with only the edges of the higlighted node
  plot_focus <- get_collab_plot(network = net_crc, crc_name = focus, crc_palette = crc_color_code, print_all_edges = F)
  ggsave(paste0("projects/crc-projects-network-", focus, "-A.png"),
         plot_focus,
         width = 10, height =10,
         units = "cm")
  # Second the edges of the complete network
  plot_focus <- get_collab_plot(network = net_crc, crc_name = focus, crc_palette = crc_color_code, print_all_edges = T)
  ggsave(paste0("projects/crc-projects-network-", focus, "-B.png"),
         plot_focus,
         width = 10, height =10,
         units = "cm")
}

