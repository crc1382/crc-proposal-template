if (!require("igraph")) install.packages("igraph")
if (!require("ggraph")) install.packages("ggraph")
setwd("plots/")

crcBaseFile <- "crc-publications-authors-edges.csv"
crcBaseFileProjects <- "crc-publications-projects-edges.csv"
crc_edges <- read.csv(crcBaseFile)
crc_edgesProjects <- read.csv(crcBaseFileProjects)


#net_crc <- simplify(graph_from_edgelist(as.matrix(crc_edges), directed = FALSE))
net_crc <- graph_from_edgelist(as.matrix(crc_edges), directed = FALSE)
net_crcProjects <- graph_from_edgelist(as.matrix(crc_edgesProjects), directed = FALSE)

# we want to count the collaborations of each project
# and have some information
crc_Info <- function(network,section){
# network: either authors or projects
# section: string for printing the name of the network
network_local <- simplify(network)
#paste0("degree: ",degree(network_local)) # The degree of a vertex is its most basic structural property, the number of its adjacent edges. 
paste0(section,":: number of vertices: ",gorder(network_local), # Order (number of vertices) of a graph
" | number of edges: ",gsize(network_local), # The size of the graph (number of edges)
" | density: ",edge_density(network_local)) # The density of a graph is the ratio of the number of edges and the number of possible edges. 
}
crc_Info(net_crc,"Authors")
crc_Info(net_crcProjects,"Projects")

# create circular layout and sort alphabetically
l <- create_layout(net_crc, layout = "linear", circular = T, offset=pi/2)
coo <- layout.circle(net_crc)
l[,1] <- coo[,1]
l[,2] <- coo[,2]


lProjects <- create_layout(net_crcProjects, 
                           layout = "linear", 
                           circular = T, 
                           offset=pi/2,
                          #sort.by=name
                           )

cooProjects <- layout.circle(net_crcProjects)
lProjects[,1] <- cooProjects[,1]
lProjects[,2] <- cooProjects[,2]



# Change the position of the label according to its position on the circle
# based on https://gist.github.com/ajhmohr/5337a5c99b504e4a243fad96203fa74f
l$angle <-with(l,ifelse(atan(-(x/y))*(180/pi) < 0,
                        90 + atan(-(x/y))*(180/pi),
                        270 + atan(-x/y)*(180/pi)))
l$hjust<-with(l,ifelse(x < 0,1.15,-0.15))


lProjects$angle <-with(lProjects,ifelse(atan(-(x/y))*(180/pi) < 0,
                        90 + atan(-(x/y))*(180/pi),
                        270 + atan(-x/y)*(180/pi)))
lProjects$hjust<-with(lProjects,ifelse(x < 0,1.15,-0.15))



of<-1.35
# g <- crc_network_directed
# edges_group = incident(crc_network, "B06")
# # Label the corresponding edges in the igraph object
# E(g)$focus <- E(g) %in% edges_group


p <- ggraph(l,
            #  sort.by=name, # does not work?!
            #circular=F,
)+
  geom_edge_arc(
    #   aes(filter=(!focus)),
    width=1,
    alpha=.3,
    color="#00549f",
    strength=1,
    #angle_calc="none",
    # n=1000,
    #lineend = "round",
    #linejoin = "mitre",
  ) + 
  geom_node_text(aes(label=name, 
                     angle=angle,
                     hjust=hjust,
  ),
  # fontface="bold",
  size= 4)+
  # scale_color_manual(values = crc_color_code)+
  coord_fixed(xlim = c(-of,of),ylim = c(-of,of))+
  theme_void()+
  coord_fixed(xlim = c(-of,of),ylim = c(-of,of))+
  theme(legend.position = "none",
        plot.margin = margin(-1,-1,-1,-1.5, "cm"),
  )



ggsave(paste0("publications/crc1382-publications-authors-network.png"),
       p,
       width = 20, height =20,
       units = "cm")


#### Now all the projects

pProjects <- ggraph(lProjects,
            #  sort.by=name, # does not work?!
            #circular=F,
)+
  geom_edge_arc(
    #   aes(filter=(!focus)),
    width=1,
    alpha=.3,
    color="#00549f",
    strength=1,
    #angle_calc="none",
    # n=1000,
    #lineend = "round",
    #linejoin = "mitre",
  ) + 
  geom_node_text(aes(label=name, 
                     angle=angle,
                     hjust=hjust,
  ),
  # fontface="bold",
  size= 9)+
  # scale_color_manual(values = crc_color_code)+
  coord_fixed(xlim = c(-of,of),ylim = c(-of,of))+
  theme_void()+
  coord_fixed(xlim = c(-of,of),ylim = c(-of,of))+
  theme(legend.position = "none",
        plot.margin = margin(-1,-1,-1,-1.5, "cm"),
  )



ggsave(paste0("publications/crc1382-publications-projects-network.png"),
       pProjects,
       width = 20, height =20,
       units = "cm")


